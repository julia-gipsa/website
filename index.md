@def title = "Julia User Group at Gipsa"
@def tags = ["syntax", "code"]


Organisers: Marco Congedo & Simon Barthelmé

## Next meeting

July 12th 2022 2PM-3.30PM, Gipsa-lab, room B314

More on Julia's type system.

## Previous sessions

See [here](sessions/) for material. 

## Mailing list 

We use a mailing list for various announcements. Subscribe by sending an empty email to sympa@grenoble-inp.fr with title "sub julia\_at\_gipsa".

## Resources



### Learning Julia 

[Documentation](https://docs.julialang.org/en/v1/)

[Julia discourse](https://discourse.julialang.org)

### Notebooks

These notebooks use [Pluto.jl](https://github.com/fonsp/Pluto.jl)

- [Introduction](notebooks/intro). Basics of Julia, and an implementation of Matching Pursuit. 
- [Julia types: basics](notebooks/types_1). Basics of Julia's type system.
