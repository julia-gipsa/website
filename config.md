<!--
Add here global page variables to use throughout your website.
-->
+++
author = "Marco Congedo, Simon Barthelmé"
mintoclevel = 2
prepath = "website"

ignore = ["node_modules/"]

# RSS (the website_{title, descr, url} must be defined to get RSS)
generate_rss = true
website_title = "Julia@Gipsa"
website_descr = "Julia User Group at Gipsa"
website_url   = "https://julia-gipsa.gricad-pages.univ-grenoble-alpes.fr/website/"
+++

<!--
Add here global latex commands to use throughout your pages.
-->
\newcommand{\R}{\mathbb R}
\newcommand{\scal}[1]{\langle #1 \rangle}
