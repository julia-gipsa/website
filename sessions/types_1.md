# Tips & Tricks, basics of types in Julia

The code shown by Marco is [here](../../notebooks/session_2_tips_and_tricks.jl).
The slides on types by Simon are [here](../julia_types_I.pdf).
The notebook for the session is available [here](../../notebooks/types_1).

For more on how Julia compiles functions, see [lecture notes](https://julia.quantecon.org/software_engineering/need_for_speed.html) by Perla, Sargent & Stachurski.

