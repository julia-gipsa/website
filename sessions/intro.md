# Links for installing Julia

## Julia itself

### Windows

Install the current stable release [here](https://julialang.org/downloads/).

### Linux & Mac OS

Install via [juliaup](https://github.com/JuliaLang/juliaup). If this doesn't work, try [jill](https://github.com/johnnychen94/jill.py), another Julia installer. If that does not work either, follow the official [installation instructions](https://julialang.org/downloads/platform/).

## Visual Studio Code

VSCode provides a development environment for Julia. 
Follow the instructions at [visualstudio.com](https://code.visualstudio.com/download).

In VSCode click on the extension icon on the left bar and type "julia" in the search bar. 
Install the "Julia" extension.

**Windows Users:** 
Make sure VSCode knows where to find Julia's executable. Go to *File->Preferences->Settings* and type "julia: exe" in the search bar. Check and edit if necessary the "Julia: Executable Path" (the executable is in the "bin" folder).

## Pluto.jl

Pluto.jl is an interactive notebook for Julia, great for beginners. To install,
open the Julia REPL, then 

```
import Pkg
Pkg.add("Pluto")
```
and run via 
```
import Pluto
Pluto.run()
```

# Basics


## Launch a REPL instance in VSCode
 **ALT+J+0** (reminder: ALT aux Jeux Olympiques)


## The four modes of the REPL
 - `]` package manager
 - `;` shell
 - `?` help, e.g., `? mean`
 - *backspace* return to julia prompt when in any other mode

## Shortcuts for running a script in VSCode
- **ALT+ENTER**
- **CTRL+A, SHIFT+ENTER**

**SHIFT+ENTER** runs a script line by line


## Package Manager

Recognizable by prompt *(environment name) pkg>*

The following commands apply to the current environment:

- `st` (or `status`): list the installed packages
- `add package1 package 2...` : Install one or more packages
    - `add StatsPlots@v0.14.32`: Install a particular version
    - `add https://github.com/Marco-Congedo/PosDefManifold.jl#dev`: Install a particular branch
- `up` (or `update`): update all installed packages
- `rm package1 package 2...`  (or `remove`): Delete one or more packages
- `gc`: garbage collector, remove obsolete dependencies

## comments

```
# inline comment
```

```
#= 
Comment Block
ipse lorum dixit
bla bla bla
=#
```

## Use packages

```
using LinearAlgebra
A=randn(3, 3)
E=eigvals(A) # eigenvalues of matrix A
```

## Use local modules
```
#=  
    add a directory in julia's the search path LOAD_PATH. 
    @__DIR__ is the directory where the current script is found
    joinpath(arg1, arg2,...) creates a path with the arguments
=#
push!(LOAD_PATH, joinpath(@__DIR__, "Modules"))

using MyModule # module MyModule.jl is in Modules directory
```

## Use script
```
# this is equivalent to copy and pasting here the content of myScript.jl
include(joinpath(@__DIR__, "myScript.jl"))
```

## strings
```
s="i am"
v="a string"
w=s*v # concatenation: i am a string
z=s^3 # repeated string concatenation

a=3
z="i saw $(a) birds" # string interpolation

println(z) # print in the REPL
```

## Conventions

- variables and functions: lower case, e.g., `a=2; a=mean(randn(3)))`
- packages and modules: upper case, e.g., `using Statistics`
- functions ending with ! modify one or more arguments, e.g., `push!`


## Set number of Threads for parallelising computations. 
In the REPL run `Sys.CPU_THREADS` and see how many logical processors your machines can use.
Go to *File->Preferences->Settings*, type "julia: Num" and edit the JSON file in "Julia Num Threads" so as to set the number of threads (e.g., to the number of logical processors).

Kill the REPL and run it again. Check that the settings is effective by running `Threads.nthreads()`.

You can also set the number of threads used by BLAS as

```
using LinearAlgebra
BLAS.set_num_threads(8)
```

# Notebook

We have made a Pluto.jl [notebook](../notebooks/intro). It introduces the basics of Julia, and an implementation of Matching Pursuit. 

