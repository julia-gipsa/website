### A Pluto.jl notebook ###
# v0.19.5

using Markdown
using InteractiveUtils

# ╔═╡ 77a69c4a-efb6-11ec-1347-0b2b0a1fd0fb
md"# Types and multiple dispatch in Julia: basics

Simon Barthelmé, Gipsa-lab

In this notebook we will explain and explore the basic features of Julia's type system, which is integral to why the language is fast (and flexible). 

## What are types for? 

A type is information available to the compiler about a variable. For example, in C, you declare the type of every variable explicitly, for instance with 
```int a;```
the compiler will know that a is an integer, and can allocate the requisite amount of memory and do the correct thing when you compute ```a+1```.

Most (all?) computer languages have types, and a way of handling them called a \"type system\". Some languages are pretty strict about type, and treat the type system as a way of detecting errors. For instance, Haskell will refuse to add a float and an integer, whereas Julia is fine with that."

# ╔═╡ 2bea07f7-9dd6-48af-982b-f0b37dba3635
md"The languages that are strict about type will not allow the user to change the type of a variable, whereas Julia is fine with that. The role of types in Julia is less about finding errors than about
- helping developers structure data and reason about the program
- helping the compiler produce efficient code

Julia is not an object-oriented language like Python or C++! The things you are used to doing with objects, you can do with the type system.
"

# ╔═╡ db6dda03-0339-4d46-8814-e9606b7d4690
md"## Finding the type of a variable

Use ```typeof``` to find the type of a variable. 
"

# ╔═╡ c9ad53fa-96a7-47fb-9a08-d10651e99a55
foo = 3.1

# ╔═╡ 76e23fb8-9928-4851-89f5-03bb70850c30
typeof(foo)

# ╔═╡ 0c8c3771-97e2-4a72-9ff9-8bcbf5d8a4a7
typeof(3)

# ╔═╡ b96b6f9b-7b18-4d67-a2ad-081d4dc718a5
typeof("afed")

# ╔═╡ 5d477fb6-7509-43af-86d6-ef499582e2c8
md"As shown above, the compiler will attribute a type automatically to certain constants. You can also create objects of a specific type by hand:"

# ╔═╡ 64351570-c5b4-4e1d-9658-5a174c20b38c
Float32(1.3)

# ╔═╡ fd4754ca-502b-42d0-aa35-4ae3780b0dfb
md"Float32 is the type for floating point numbers with 32 bits of precision. The default in Julia is Float64, but Float32 computation is faster and may be useful for low-precision computations. 

## Automatic Conversions and Promotions

Julia lets you add numbers with different types. When you add a float to an int, you have to decide whether the result you want is a float or an int. Julia gives you floats: "

# ╔═╡ 1a376e4f-ece9-44d0-9230-fc793f24c1ab
md"A related issue is what happens when you divide an integer by an integer. The result isn't an integer! Julia gives a float. 
"

# ╔═╡ 16343241-51aa-4128-8bb7-d7f26e67550b
typeof(3/5)

# ╔═╡ ef4d2e44-4c6a-4add-86bf-30ef8b4c8b9f
md"However, you can choose to define a Rational instead, a number that is represented as the ratio of two Ints." 

# ╔═╡ 446ee343-6ff6-4880-85ec-36bc8e513bc3
typeof(3 // 5)

# ╔═╡ 6903f30d-6946-437e-b570-82791ccadc95
md"Compare and contrast:"

# ╔═╡ 65b0c0ec-be77-4cb6-9bea-1b3abc3e8982
(3 // 5) / 2 

# ╔═╡ 5ea0c71c-d76c-42f8-9775-92a3cbccdf3f
(3//5) / 2.0

# ╔═╡ 443b251e-29c6-44bb-b1be-b714742b4856
md"Pay attention to all the different numerical types in Julia, this will pay off when you start to work with so-called parametric types (like vectors and matrices).

# Julia's compilation model and multiple dispatch

Here's the most important thing you have to understand about Julia: Julia compiles functions on demand based on the type of the arguments. 

Let's define a function:"

# ╔═╡ ab562d46-4071-45c3-ac4a-f64692ca6960
md"We can call that function with ints, floats, etc. The first time we call foo(x,y), Julia will need to compile the function. Crucially, it will produce different, specialised code, depending on the type of the arguments!"

# ╔═╡ 3e084864-bfe8-447f-b7a3-775adf3aa82d
md"When first encountering each of these calls, Julia looks at the type of the arguments. It checks to see whether it's already got an appropriate version of the function compiled in the cache, and if not it compiles one on-the-fly. The version that has two ints as input will be faster than the the one that has two floats!

If you're not convinced the machine code is different, feel free to examine it using the @code_native macro, which displays assembly (CPU-level) code. 
" 

# ╔═╡ 4b6cf11f-9180-42e1-8b39-3ba5f6977821
md"# Specialisation and multiple dispatch

Internally, the type Rational{Int64} works by representing a rational number in the obvious way, i.e. r = a/b is represented as two ints, a and b. This is simple enough but calls for a different way of implementing multiplication and addition, compared to a simple Float64! 

When we call Bar(6//2, 3//2), the compiler encounters a multiplication in the definition of the function. Multiplication of two numbers in Julia is just another function, and the compiler checks whether if that function is indeed defined somewhere for rationals. Same goes for addition. 

The function for multiplication is just *
"

# ╔═╡ 7f6ce4a0-8f16-4077-b2c4-e6a2eff3fdb9
md"Let's take a look at the definition of the multiplication function for rationals. Most of base Julia is in Julia, so it's easy to examine. The right definition can be found by running ```@edit *(2//3,1//3)``` if you're curious. The definition, from base/rational.jl, reads:
```
function *(x::Rational, y::Rational)
    xn, yd = divgcd(promote(x.num, y.den)...)
    xd, yn = divgcd(promote(x.den, y.num)...)
    unsafe_rational(checked_mul(xn, yn), checked_mul(xd, yd))
end
```
Can you see why things are implemented that way?

We can contrast the definition for rationals with the definition for complex numbers:
```
*(z::Complex, w::Complex) = Complex(real(z) * real(w) - imag(z) * imag(w),
                                    real(z) * imag(w) + imag(z) * real(w))
```

Hopefully, at this point, some of the logic behind Julia should be apparent. 

- We can define functions with different levels of genericity. 
- Our first function (\"bar\") had no type annotations and was completely generic  
- It contains multiplication and addition operations which are other function calls pretending to be binary operators.
- When compiling bar(2 // 1, 4 // 2) the compiler figures out that the input types are rationals, and when it encounters a *(Rational, Rational) operation it looks for a definition of that operation, called a specialisation. For bar(2,4) the specialisation is *(Int64,Int64) and is different. 

The idea that functions may have different specialised versions depending on input type, with the compiler calling the most appropriate version, is called *multiple dispatch*. 

The specialised versions of functions are called \"methods\" in Julia lingo.

Now let's implement our own type for complex numbers!

## User-defined types 

You can define your own types. As a toy example, let's define our own type for complex numbers. 
"

# ╔═╡ 43ee4e51-5162-40da-9dda-927ea066f1ae
begin 

# a complex number a + ib
struct Cpx 
	a :: Float64
	b :: Float64
end

#Our new type comes automatically with a constructor function. The default constructor takes (a,b) as input.
#We can also specialise our constructor: if the constructor has a single argument, we assume the imaginary part is null
function Cpx(a) 
	Cpx(a,0.0)
end

	
end

# ╔═╡ 8f8cd090-7e1d-44e5-9e8d-8e81a024f2ab
Cpx(1.0,2.0)

# ╔═╡ 47dd258d-5102-4fb5-bc3f-c02b9254a649
Cpx(1.0)

# ╔═╡ f16a9db2-cc29-479c-9739-4c11c8bde8ef
md"Now let's define how addition should work by *specialising* the base function +(a,b)"

# ╔═╡ b0d6b79a-c8ef-42a8-acfd-4ea5e3e5b377
begin 
	import Base:+ #needed for extending the Base:+ function 
	#otherwise a new independent function is defined in the current module 
	function +(z1 :: Cpx,z2 :: Cpx) 
		Cpx(z1.a+z2.a,z1.b+z2.b)
	end
end

# ╔═╡ d019ad4a-f7ab-4d60-a4fd-b94664c6fc3f
1.3+2

# ╔═╡ 69054d88-91ed-4d24-a461-07bf8aef8a6b
typeof(3+0.1)

# ╔═╡ bede3123-03ee-4496-a59c-375e251e5748
typeof(3+2.0) #Even though the result is an integer!

# ╔═╡ 298ec2e0-7ac4-4cbb-9ea9-709bdc9c18f8
Cpx(1.2,1.3)+Cpx(2.0,2.1)

# ╔═╡ 38a10fa4-b662-48da-bf5b-fbe1593fc59d
md"We can do multiplication as well:"

# ╔═╡ 9b7c322c-b3fe-49a0-a6b8-114bb301f2b9
begin 
	import Base:* 
	function *(z1 :: Cpx,z2 :: Cpx) 
		Cpx(z1.a*z2.a - z1.b*z2.b,z1.b*z2.a + z2.b*z1.a)
	end
end

# ╔═╡ 81a85ad7-7612-47f2-95ac-b85e82a790b6
function bar(x,y)
	return x*y+x+y
end

# ╔═╡ 02525569-588b-4a2a-bb1c-1f83f9c71fbc
bar(3,2)

# ╔═╡ e3434eed-8901-4bcd-8af7-cb34d34ef5e0
bar(3.0,2.0)

# ╔═╡ 3c24427c-9ea1-4931-94c6-8de408a98791
bar(2//3,4//5)

# ╔═╡ f865f925-1c3d-4898-833a-ffedbf997a91
@code_native bar(3,2)

# ╔═╡ 4e2553b9-4cbb-45e8-9133-e68cecb101e9
@code_native bar(3.0,2.0)

# ╔═╡ fe8ebdcf-5388-498e-b5cc-f64ef530eed1
*(3,2)

# ╔═╡ b92ff3a4-82ba-4244-b96c-6a7d0a9ec32f
*(2//3,5//3)

# ╔═╡ 4682e9af-8fce-4f05-bf98-f6d36bb57545
Cpx(3,2)*Cpx(3,1)

# ╔═╡ dc72b4a3-5996-466a-bace-e3e4a7788fe4
#remember function bar from before? it now has everything it needs to run on our new complex type!
bar(Cpx(3,2),Cpx(2,1))

# ╔═╡ 447fa145-8cdd-47f2-9ac5-7f6cbb32d82d
md"# Conclusion

We've seen the basics of how Julia types are defined, and how the compiler uses that information to look up appropriate methods and compile them. Multiple dispatch lets you define a single generic function, and specialise it as needed when you have additional information on your data. For instance, you can use that mechanism to have different versions of a matrix factorisation, one for general matrices and one for symmetric matrices. Maybe you need a different specialisation for matrices of complex numbers, and multiple dispatch lets you define that. It is a powerful mechanism, but it may take a while until you get used to what it can do.  

Next time, we'll see how to use the type system to write performant code. We'll talk about parametric types, type stability and type inference. 

# Exercises 

- Define additional functions for our Cpx type, for instance division and exponentiation
- Extending Base.show lets you define a custom way for Julia to display a variable. It should be of the form 
```
function show(io::IO,x::Cpx)
end
```
and return a string. See if you can write one that displays Cpx numbers as 
```
2.99 + i×3.02
```
- If you're feeling like more of a challenge, look up some documentation on promote_rule, so that you can easily add a Cpx and a float!
"

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╟─77a69c4a-efb6-11ec-1347-0b2b0a1fd0fb
# ╠═d019ad4a-f7ab-4d60-a4fd-b94664c6fc3f
# ╟─2bea07f7-9dd6-48af-982b-f0b37dba3635
# ╟─db6dda03-0339-4d46-8814-e9606b7d4690
# ╠═c9ad53fa-96a7-47fb-9a08-d10651e99a55
# ╠═76e23fb8-9928-4851-89f5-03bb70850c30
# ╠═0c8c3771-97e2-4a72-9ff9-8bcbf5d8a4a7
# ╠═b96b6f9b-7b18-4d67-a2ad-081d4dc718a5
# ╟─5d477fb6-7509-43af-86d6-ef499582e2c8
# ╠═64351570-c5b4-4e1d-9658-5a174c20b38c
# ╟─fd4754ca-502b-42d0-aa35-4ae3780b0dfb
# ╠═69054d88-91ed-4d24-a461-07bf8aef8a6b
# ╠═bede3123-03ee-4496-a59c-375e251e5748
# ╟─1a376e4f-ece9-44d0-9230-fc793f24c1ab
# ╠═16343241-51aa-4128-8bb7-d7f26e67550b
# ╟─ef4d2e44-4c6a-4add-86bf-30ef8b4c8b9f
# ╠═446ee343-6ff6-4880-85ec-36bc8e513bc3
# ╟─6903f30d-6946-437e-b570-82791ccadc95
# ╠═65b0c0ec-be77-4cb6-9bea-1b3abc3e8982
# ╠═5ea0c71c-d76c-42f8-9775-92a3cbccdf3f
# ╟─443b251e-29c6-44bb-b1be-b714742b4856
# ╠═81a85ad7-7612-47f2-95ac-b85e82a790b6
# ╟─ab562d46-4071-45c3-ac4a-f64692ca6960
# ╠═02525569-588b-4a2a-bb1c-1f83f9c71fbc
# ╠═e3434eed-8901-4bcd-8af7-cb34d34ef5e0
# ╠═3c24427c-9ea1-4931-94c6-8de408a98791
# ╟─3e084864-bfe8-447f-b7a3-775adf3aa82d
# ╠═f865f925-1c3d-4898-833a-ffedbf997a91
# ╠═4e2553b9-4cbb-45e8-9133-e68cecb101e9
# ╟─4b6cf11f-9180-42e1-8b39-3ba5f6977821
# ╠═fe8ebdcf-5388-498e-b5cc-f64ef530eed1
# ╠═b92ff3a4-82ba-4244-b96c-6a7d0a9ec32f
# ╟─7f6ce4a0-8f16-4077-b2c4-e6a2eff3fdb9
# ╠═43ee4e51-5162-40da-9dda-927ea066f1ae
# ╠═8f8cd090-7e1d-44e5-9e8d-8e81a024f2ab
# ╠═47dd258d-5102-4fb5-bc3f-c02b9254a649
# ╟─f16a9db2-cc29-479c-9739-4c11c8bde8ef
# ╠═b0d6b79a-c8ef-42a8-acfd-4ea5e3e5b377
# ╠═298ec2e0-7ac4-4cbb-9ea9-709bdc9c18f8
# ╟─38a10fa4-b662-48da-bf5b-fbe1593fc59d
# ╠═9b7c322c-b3fe-49a0-a6b8-114bb301f2b9
# ╠═4682e9af-8fce-4f05-bf98-f6d36bb57545
# ╠═dc72b4a3-5996-466a-bace-e3e4a7788fe4
# ╟─447fa145-8cdd-47f2-9ac5-7f6cbb32d82d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
