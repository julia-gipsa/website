### A Pluto.jl notebook ###
# v0.19.5

using Markdown
using InteractiveUtils

# ╔═╡ b6071ef1-f54c-495b-852d-7df9d6ecd5ee
begin
	using Plots
end

# ╔═╡ 77ce0322-71ea-444c-8e48-f1a7b52b02d0
using LinearAlgebra #basic LA support

# ╔═╡ 77aedba3-c2c6-430a-9748-f529afb17604
html"<button onclick='present()'>present</button>"

# ╔═╡ 9f452e56-de74-11ec-0ef3-6fc65e92a070
md"# Getting started with Julia

In this tutorial, we'll cover some of the basics. Our goal is to implement a classic algorithm in signal processing called [Matching Pursuit](https://en.wikipedia.org/wiki/Matching_pursuit). First we'll look at how to compute some simple things, how to use vectors and how to make basic plots."

# ╔═╡ 5b519f65-6894-40ff-a460-b7f4304e066d
md"
## Pluto.jl

The environment we are using is called [Pluto.jl](https://github.com/fonsp/Pluto.jl), a Julia notebook in the spirit of Jupyter (but different). We can enter Julia code directly into the cells and run it. The result is displayed next to the code cells."

# ╔═╡ 182ec828-ba1e-4392-b4b7-9df494cf9641
a = 1.0 #defining a variable

# ╔═╡ bbb64f48-b403-43ec-bd6e-94c042ccadda
a + 2

# ╔═╡ 3149284c-ba3c-4a21-8870-40b5ad8780b3
sin(a)

# ╔═╡ dec55c6b-336d-4a14-ab24-ca877fb45112
md"Press Shift+Enter to evaluate a cell, Ctrl+Enter to evaluate and insert a new cell, Delete to delete a cell. Notice the \"live docs\" panel: you can use it to lookup functions"

# ╔═╡ b7adcff7-b5bf-437c-a48b-b0bf28b7298a
md"Pluto.jl is a dynamic notebook: change the value of ``a`` and every cell that depends on ``a`` gets updated as well!"

# ╔═╡ f2a602ec-1179-4d6f-90a1-97174db2b263

md"## Types
Here we have defined a variable and done some computations, in a way that should be familiar from other interactive languages. 
Importantly, in Julia variables have types. For example, ```a``` is of type Float64.
"

# ╔═╡ 47aaa4f3-27d4-4b0e-a439-6c2b441cc4dd
typeof(a)

# ╔═╡ 483f0d3f-74d6-4e06-84e2-9bec1ce4fb1f
md"Note the following:"

# ╔═╡ d298e0cc-98aa-430b-b069-408f784292f7
typeof(1)

# ╔═╡ 7d5327b0-e339-4c1b-a67a-af647df3ff4d
typeof(1.0)

# ╔═╡ cf1d6abd-4a58-4612-984e-35d9fc05546c
typeof(1.3e4)

# ╔═╡ 8a2c18b7-f7ea-46f2-a089-562443438dc1
md"Numerical constants (typically) have type Float64 or Int64. 64 is short-hand for 64 bits, which the size of these objects in memory. In Julia types matter a lot and automatic type conversion is not as systematic as in other languages. For instance, the factorial function is defined for integers, but not floats!"

# ╔═╡ 09a48134-e578-4b0e-8b16-a69fe865020c
factorial(4)

# ╔═╡ 9af7ca8d-02e6-4978-914c-64890813ee66
factorial(4.0)

# ╔═╡ 6ea9c88f-32eb-4a3e-97d3-06587d5825c6
md"The somewhat cryptic message above says that the Julia compiler can't find a definition for the factorial function that would work on an input of type Float64. We'll have much more to say about types later on, for now just be aware that they exist, and that they are important."

# ╔═╡ 581fa9b9-0c99-4c50-aff7-98f6233d196b
md"## Native Unicode support"

# ╔═╡ b5831e4a-ff3c-41b0-89ea-489984234041
md"Julia has two (slightly) unusual features where variables are concerned. First, Unicode is supported everywhere, so you have a lot of choice in how to name your variables"

# ╔═╡ 2d99bd64-bc34-4d67-9b88-a1f46c87a66f
α = 5.5; β̂ = .3; γ₄=3;

# ╔═╡ 8d20ec1c-1238-49f9-9dd1-4f220d475e21
md"To type these characters into Pluto.jl, use Latex syntax and then Tab. For instance, the first one can be obtained as \alpha then Tab. The hat is \hat, the underscore is \\_"

# ╔═╡ e3649590-d547-4e77-879a-477b0599f4f5
md"Another cosmetic feature that looks unusual is support of multiplication by directly putting a number before a variable's name, as in:"

# ╔═╡ f2b358f5-e327-4958-b72e-329559c27af0
2α

# ╔═╡ b1a21961-beb2-4b10-afb0-3fd8e2723213
3.2β̂

# ╔═╡ 8d8f3e49-c9a9-4f61-9954-0d071b78610f
2*α #same

# ╔═╡ e8387545-1bf0-41fc-8ff7-90699dc7b1ae
md"At any point, you can change the value in the code cells to experiment, so feel free to do that!"

# ╔═╡ 80c3b15c-4cb4-4df9-9efd-10889394e0cc
md"
## Vectors 

Now let's look at vectors and plot things. Julia has many different plotting libraries (unfortunately), but here we'll stick with the historical one, called Plots.jl."

# ╔═╡ 34a81ebd-e509-41bb-a66a-e3a93c3f9752
v = [.1,.5,.3]

# ╔═╡ a1eed0e7-ecb6-48a5-850d-ec683f5157bd
3*v

# ╔═╡ a8a89f73-689c-4004-a87e-72cb9d9d3c2b
2*v + [1,2,3] #multiply, and add a vector

# ╔═╡ e337fa1a-95d6-44ff-903a-caa9d3c9b7c5
v + 3

# ╔═╡ 91405eab-c2b0-405e-9985-58bcb79037aa
md"Here we have defined a vector (of floats) of length 3, then multiplied by a constant. We then attempted to add 3 to the vector, which the compiler refuses to do. Can you guess why?"

# ╔═╡ 0181b2a9-5fd6-4d4a-9a4e-18794f034dc3
md"## Broadcasting and the dot syntax 
Julia tends to err on the side of mathematical rigor: it doesn't make sense to add a scalar to a vector. In most cases what we mean is 'add the same value to all the entries'. Julia does that with a ."

# ╔═╡ 53b78dab-667a-4a87-8f80-10e114082924
v .+ 3

# ╔═╡ 4ae709e0-c3eb-4b38-8b14-b6fa7e4b88b9
md"The dot is used to make operations apply element-wise. It is inspired by Matlab syntax, but much more general." 

# ╔═╡ e5e8868f-a09d-454a-9bd0-fc6639e92b52
sin.(v)

# ╔═╡ 979d9ce5-104f-48c4-8292-c3582e7bb87e
((u) -> exp(-3*u+2)*sin(u)).(v) #can you guess what this syntax does?

# ╔═╡ 45ec92dd-8cab-410a-9af9-314024247cac
md"Broadcasting is quite flexible, "

# ╔═╡ a344797d-1e1e-42e9-9b75-320c521781d6
md"Now let's plot a sine wave. A good way of producing a vector that ranges from a to b with n linear steps is to use range:"

# ╔═╡ 82b72675-4938-4feb-8965-d76ee22ceaed
t = range(0,1,300) 

# ╔═╡ a59467b2-e98a-4589-be43-667951039c88
plot(t,sin.(2π * t))

# ╔═╡ 8b247a46-64f2-402a-9a60-d5fc44ddb7e5
md"Sometimes typing dots is a pain and you wish for a shortcut. The @. macro puts dots everywhere. Macros are an advanced topic but what @. does should be clear from a few examples"

# ╔═╡ e11c1475-616d-46f7-aa18-4e29fe55a959
plot(t,exp.(-2t.^2) .* sin.(4π * t))

# ╔═╡ 7d028810-d9df-49a0-b29a-08a72e9509b9
plot(t, @. exp(-2t^2)*sin(4π * t))

# ╔═╡ 68124cfa-980b-4831-9e5b-e255330e6a8e
md" ## Matrices and Linear Algebra

Matrices and Linear Algebra work mostly like you'd expect coming from Matlab. Use dots again for element-wise operations."

# ╔═╡ 12873361-902d-4a6f-91bd-6b55c9ea3418
A = [1 2; 3 4] #you can enter matrices by row

# ╔═╡ 3f772d14-db12-4921-8965-f2042782126e
hcat([1,2],[3,4]) #concatenate column vectors horizontally

# ╔═╡ a031c9fe-5aa6-4c8b-bfe4-cdf9429336c7
vcat([1 2],[3 4]) #row vectors vertically

# ╔═╡ 75d6aad1-0d2d-418d-b954-665e450e8abe
hcat(A,[5,6]) #concatenation of a matrix and a row vector

# ╔═╡ 6a2600a1-8e0a-4a95-a72d-120626308530
hcat(A,[5,6]) .+ 1

# ╔═╡ 136d4a2f-66e8-429a-9c12-2f50f7d3ec9a
z=randn(2) #a random vector with standard Gaussian entries

# ╔═╡ b760ec6e-4be9-4221-8d83-5f66507f8c70
A*z #matrix vector multiply

# ╔═╡ 672888b3-20b9-4243-b36d-cc32079516cd
z'*z

# ╔═╡ 45dd6386-8480-476f-a176-b80ff20a932b
z*z'

# ╔═╡ 46ba433f-8459-4083-a56a-11d1869470c6
md"Solve linear systems using the backslash operator."

# ╔═╡ 5f51a124-e1ea-4762-83c6-d55540e69d9c
A\z

# ╔═╡ 6cfaf34a-0274-4c1d-af85-3f4c1186e2f4
A*(A\z)

# ╔═╡ ac56b8b7-19a8-4e92-b04c-e94a8076753a
A*(A\z) .≈ z #guess what this does!

# ╔═╡ fa19ede0-d5e7-4d7c-b973-1e375705e91d
md"
# Implementing Matching Pursuit

Matching Pursuit is a classical algorithm in Signal Processing by Mallat & Zhang (1993), a variant of stepwise regression. It is used to find a sparse representation of a signal in a dictionary. A dictionary is a large collection of basis functions
``\phi_1(t), \phi_2(t), \ldots, \phi_m(t) ``.
The goal is to express a signal ``x(t)`` as 

```math
x(t) = \sum_{i=1}^m \alpha_i \phi_i(t)
```

where most of the coefficients are zero.
Chances are, you have already seen this type of problem (most likely, a dozen times at least). Matching pursuit proceeds in a greedy fashion, by finding the basis function that correlates most with ``x(t)``, then updating ``\alpha``, then finding the basis function that correlates most with the residual, updating ``\alpha`` again, etc. 

Let's first see how to build a dictionary. We'll use so-called Gabor functions. A Gabor function is the product of a Gaussian and a sine wave. 
"

# ╔═╡ d1685688-bfdf-45ba-b485-30a2f5aaf242
#θ is phase, σ is std. dev of Gaussian, ω is freq. of sine wave
function gabor(t,θ=0.0,σ=1.0,ω=1.0)
	τ = t - θ #time relative to phase
 	return exp(-.5(τ/(σ))^2)*cos(2π*τ*ω)		
end

# ╔═╡ 4895cf21-31eb-4be9-b5e8-144d782d5dbb
md"We are now going to plot different Gabor functions using different tricks. First up, a loop."

# ╔═╡ f25a144f-bed1-481d-8f4f-26855d0c4433
begin #don't worry about the begin..end stuff for now
	n = length(t)
	g = zeros(n)
	for i in 1:n
		g[i] = gabor(t[i],.5,.1,8)
	end
	plot(t,g)
end

# ╔═╡ e8a93ec5-f4f5-49c2-ac5b-b0a9e190830a
md"Next, a Python-like 'array comprehension'"

# ╔═╡ b21cbf1e-fd29-4aa3-ba81-d2fb118baceb
begin 
	h = [gabor(tt,.5,.2,8) for tt in t]
	plot(t,h)
end

# ╔═╡ 56cf05e3-6a68-48be-a5ac-173e76cd3490
md"Next, it's anonymous functions and the dot operator again"

# ╔═╡ 678817c8-1008-4d5e-b748-35cea0f9b79e
begin 
	fun = (v) -> gabor(v,.8,.2,6)
	plot(t,fun.(t))
end

# ╔═╡ c09ae70c-0ce8-4c1b-a022-71ee8b9b9d0e
md"Our function for building a dictionary of Gabor functions will take as input a time vector, a vector of frequencies, a vector of phases, and a vector of std. deviations. 

We build a dictionary by going through every combination of freq., phase and std. deviations. The output is a ``n \times m`` matrix, where ``n`` is the number of time samples and ``m`` is the size of the dictionary."

# ╔═╡ 9723bce0-a749-40f9-9083-9246f94cd7cc
function build_dict(t,θ,σ,ω)
	D = zeros(length(t),length(θ)*length(σ)*length(ω))
	i = 1	
	for ph in θ
		for s in σ
			for freq in ω
				g = [gabor(tt,ph,s,freq) for tt in t]
				D[:,i] = g/norm(g)
				i+=1
			end
		end
	end
	D
end

# ╔═╡ 4592285a-6a27-4a0e-a339-1b7dc32cb8f5
D=build_dict(t,range(0,1,10),range(.01,.1,10),range(1,20,30));

# ╔═╡ 238447dd-ca0e-401d-b7ea-cbefd621c81e
plot(t,D[:,[125,430,1700,2933]])

# ╔═╡ e3e28d3e-ab95-4a85-b6fc-8f00303c75ca
heatmap(D[:,1:400])

# ╔═╡ 23ba5bba-82a2-41c2-9e74-c369a7e44743
md"### Building a test signal

The Matching Pursuit algorithm is sometimes used in time-frequency analysis. Let's build a 'chirp' signal, whose frequency increases over time. 

"

# ╔═╡ 5e2655e3-72c3-4759-b445-13976d9c4904
function chirp(t,ω0=1,ω1=10)
	freq = ω0*(1-t)+ω1*t
	cos(t*freq)*exp(-3*t)
end

# ╔═╡ 9b7d8769-c571-448d-afc0-59ea58258557
plot(t,((v) -> chirp(v,1,50)).(t))

# ╔═╡ 55ea8a8c-5953-48d7-8d50-592e3e052575
plot(t,((v) -> chirp(v,60,80)).(t))

# ╔═╡ 7851e90d-5bea-4ce0-85a0-a428e62bd78d
md"## Implementing Matching Pursuit

We are now ready to implement MP. Take some time to look at the code below, most of the syntax should be familiar. 
"

# ╔═╡ 80965dae-2a75-41b9-885a-8da9b8e42125
# y is the signal (vector of length n), D is a n × m matrix  
function matching_pursuit(y,D,maxiter=100)
	n,m = size(D)
	@assert length(y) == n #check input
	res = copy(y) #vector of residuals
	α = zeros(m) #vector of coeffs
	for iter in 1:maxiter
		dp = D'*res #dot product of residual with every function in dict
		val,i = findmax(abs.(dp)) #find the one that most correlates with residual
		α[i] += dp[i] #update coeff
		res -= D[:,i]*dp[i] #update residual
	end
	α
end

# ╔═╡ bed572d5-8ca3-4bbb-b9d1-d14b7f685edf
md"We now generate a test signal"

# ╔═╡ cb64ca85-4d00-42db-8873-410c3ca5197e
zz = ((v) -> chirp(v,1,50)).(t);

# ╔═╡ a6b81141-8d03-4268-a9ae-5f513a0c8f02
plot(t,zz)

# ╔═╡ 9c7a7201-eac0-46e7-b757-f0a981b68214
α_est=matching_pursuit(zz,D,55);

# ╔═╡ 7ac1c525-801c-4b19-9708-7b799a1392fe
plot(α_est)

# ╔═╡ fb70c442-0b69-4505-b147-c33553edc917
plot([zz D*α_est],labels=["signal" "reconstruction"])

# ╔═╡ 8e8cc87a-cff1-4d78-b34e-0cd3b3d72de6
md"# Exercises

* Modify the matching_pursuit function to have a better stopping criterion; for instance make it stop iterating if the norm of the residuals goes below a threshold. You can use a while loop or keep the for loop but add a break statement.   
* Implement Orthogonal Matching Pursuit, an improved variant
* Use the ProfileViewer package to find which lines in the code are the most time-consuming, then think of ways of speeding up these parts. 
"

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
Plots = "91a5bcdd-55d7-5caf-9e0b-520d859cae80"

[compat]
Plots = "~1.29.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.Adapt]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "af92965fb30777147966f58acb05da51c5616b5f"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.3.3"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Bzip2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "19a35467a82e236ff51bc17a3a44b69ef35185a2"
uuid = "6e34b625-4abd-537c-b88f-471c36dfa7a0"
version = "1.0.8+0"

[[deps.Cairo_jll]]
deps = ["Artifacts", "Bzip2_jll", "Fontconfig_jll", "FreeType2_jll", "Glib_jll", "JLLWrappers", "LZO_jll", "Libdl", "Pixman_jll", "Pkg", "Xorg_libXext_jll", "Xorg_libXrender_jll", "Zlib_jll", "libpng_jll"]
git-tree-sha1 = "4b859a208b2397a7a623a03449e4636bdb17bcf2"
uuid = "83423d85-b0ee-5818-9007-b63ccbeb887a"
version = "1.16.1+1"

[[deps.ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "9489214b993cd42d17f44c36e359bf6a7c919abf"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "1.15.0"

[[deps.ChangesOfVariables]]
deps = ["ChainRulesCore", "LinearAlgebra", "Test"]
git-tree-sha1 = "1e315e3f4b0b7ce40feded39c73049692126cf53"
uuid = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
version = "0.1.3"

[[deps.ColorSchemes]]
deps = ["ColorTypes", "ColorVectorSpace", "Colors", "FixedPointNumbers", "Random"]
git-tree-sha1 = "7297381ccb5df764549818d9a7d57e45f1057d30"
uuid = "35d6a980-a343-548e-a6ea-1d62b119f2f4"
version = "3.18.0"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "0f4e115f6f34bbe43c19751c90a38b2f380637b9"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.3"

[[deps.ColorVectorSpace]]
deps = ["ColorTypes", "FixedPointNumbers", "LinearAlgebra", "SpecialFunctions", "Statistics", "TensorCore"]
git-tree-sha1 = "d08c20eef1f2cbc6e60fd3612ac4340b89fea322"
uuid = "c3611d14-8923-5661-9e6a-0046d554d3a4"
version = "0.9.9"

[[deps.Colors]]
deps = ["ColorTypes", "FixedPointNumbers", "Reexport"]
git-tree-sha1 = "417b0ed7b8b838aa6ca0a87aadf1bb9eb111ce40"
uuid = "5ae59095-9a9b-59fe-a467-6f913c188581"
version = "0.12.8"

[[deps.Compat]]
deps = ["Dates", "LinearAlgebra", "UUIDs"]
git-tree-sha1 = "924cdca592bc16f14d2f7006754a621735280b74"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.1.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Contour]]
deps = ["StaticArrays"]
git-tree-sha1 = "9f02045d934dc030edad45944ea80dbd1f0ebea7"
uuid = "d38c429a-6771-53c6-b99e-75d170b6e991"
version = "0.5.7"

[[deps.DataAPI]]
git-tree-sha1 = "fb5f5316dd3fd4c5e7c30a24d50643b73e37cd40"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.10.0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "d1fff3a548102f48987a52a2e0d114fa97d730f0"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.13"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "b19534d1895d702889b219c382a6e18010797f0b"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.8.6"

[[deps.Downloads]]
deps = ["ArgTools", "FileWatching", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.EarCut_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "3f3a2501fa7236e9b911e0f7a588c657e822bb6d"
uuid = "5ae413db-bbd1-5e63-b57d-d24a61df00f5"
version = "2.2.3+0"

[[deps.Expat_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "bad72f730e9e91c08d9427d5e8db95478a3c323d"
uuid = "2e619515-83b5-522b-bb60-26c02a35a201"
version = "2.4.8+0"

[[deps.FFMPEG]]
deps = ["FFMPEG_jll"]
git-tree-sha1 = "b57e3acbe22f8484b4b5ff66a7499717fe1a9cc8"
uuid = "c87230d0-a227-11e9-1b43-d7ebe4e7570a"
version = "0.4.1"

[[deps.FFMPEG_jll]]
deps = ["Artifacts", "Bzip2_jll", "FreeType2_jll", "FriBidi_jll", "JLLWrappers", "LAME_jll", "Libdl", "Ogg_jll", "OpenSSL_jll", "Opus_jll", "Pkg", "Zlib_jll", "libass_jll", "libfdk_aac_jll", "libvorbis_jll", "x264_jll", "x265_jll"]
git-tree-sha1 = "d8a578692e3077ac998b50c0217dfd67f21d1e5f"
uuid = "b22a6f82-2f65-5046-a5b2-351ab43fb4e5"
version = "4.4.0+0"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Fontconfig_jll]]
deps = ["Artifacts", "Bzip2_jll", "Expat_jll", "FreeType2_jll", "JLLWrappers", "Libdl", "Libuuid_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "21efd19106a55620a188615da6d3d06cd7f6ee03"
uuid = "a3f928ae-7b40-5064-980b-68af3947d34b"
version = "2.13.93+0"

[[deps.Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[deps.FreeType2_jll]]
deps = ["Artifacts", "Bzip2_jll", "JLLWrappers", "Libdl", "Pkg", "Zlib_jll"]
git-tree-sha1 = "87eb71354d8ec1a96d4a7636bd57a7347dde3ef9"
uuid = "d7e528f0-a631-5988-bf34-fe36492bcfd7"
version = "2.10.4+0"

[[deps.FriBidi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "aa31987c2ba8704e23c6c8ba8a4f769d5d7e4f91"
uuid = "559328eb-81f9-559d-9380-de523a88c83c"
version = "1.0.10+0"

[[deps.GLFW_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libglvnd_jll", "Pkg", "Xorg_libXcursor_jll", "Xorg_libXi_jll", "Xorg_libXinerama_jll", "Xorg_libXrandr_jll"]
git-tree-sha1 = "51d2dfe8e590fbd74e7a842cf6d13d8a2f45dc01"
uuid = "0656b61e-2033-5cc2-a64a-77c0f6c09b89"
version = "3.3.6+0"

[[deps.GR]]
deps = ["Base64", "DelimitedFiles", "GR_jll", "HTTP", "JSON", "Libdl", "LinearAlgebra", "Pkg", "Printf", "Random", "RelocatableFolders", "Serialization", "Sockets", "Test", "UUIDs"]
git-tree-sha1 = "b316fd18f5bc025fedcb708332aecb3e13b9b453"
uuid = "28b8d3ca-fb5f-59d9-8090-bfdbd6d07a71"
version = "0.64.3"

[[deps.GR_jll]]
deps = ["Artifacts", "Bzip2_jll", "Cairo_jll", "FFMPEG_jll", "Fontconfig_jll", "GLFW_jll", "JLLWrappers", "JpegTurbo_jll", "Libdl", "Libtiff_jll", "Pixman_jll", "Pkg", "Qt5Base_jll", "Zlib_jll", "libpng_jll"]
git-tree-sha1 = "1e5490a51b4e9d07e8b04836f6008f46b48aaa87"
uuid = "d2c73de3-f751-5644-a686-071e5b155ba9"
version = "0.64.3+0"

[[deps.GeometryBasics]]
deps = ["EarCut_jll", "IterTools", "LinearAlgebra", "StaticArrays", "StructArrays", "Tables"]
git-tree-sha1 = "83ea630384a13fc4f002b77690bc0afeb4255ac9"
uuid = "5c1252a2-5f33-56bf-86c9-59e7332b4326"
version = "0.4.2"

[[deps.Gettext_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Libiconv_jll", "Pkg", "XML2_jll"]
git-tree-sha1 = "9b02998aba7bf074d14de89f9d37ca24a1a0b046"
uuid = "78b55507-aeef-58d4-861c-77aaff3498b1"
version = "0.21.0+0"

[[deps.Glib_jll]]
deps = ["Artifacts", "Gettext_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Libiconv_jll", "Libmount_jll", "PCRE_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "a32d672ac2c967f3deb8a81d828afc739c838a06"
uuid = "7746bdde-850d-59dc-9ae8-88ece973131d"
version = "2.68.3+2"

[[deps.Graphite2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "344bf40dcab1073aca04aa0df4fb092f920e4011"
uuid = "3b182d85-2403-5c21-9c21-1e1f0cc25472"
version = "1.3.14+0"

[[deps.Grisu]]
git-tree-sha1 = "53bb909d1151e57e2484c3d1b53e19552b887fb2"
uuid = "42e2da0e-8278-4e71-bc24-59509adca0fe"
version = "1.0.2"

[[deps.HTTP]]
deps = ["Base64", "Dates", "IniFile", "Logging", "MbedTLS", "NetworkOptions", "Sockets", "URIs"]
git-tree-sha1 = "0fa77022fe4b511826b39c894c90daf5fce3334a"
uuid = "cd3eb016-35fb-5094-929b-558a96fad6f3"
version = "0.9.17"

[[deps.HarfBuzz_jll]]
deps = ["Artifacts", "Cairo_jll", "Fontconfig_jll", "FreeType2_jll", "Glib_jll", "Graphite2_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Pkg"]
git-tree-sha1 = "129acf094d168394e80ee1dc4bc06ec835e510a3"
uuid = "2e76f6c2-a576-52d4-95c1-20adfe4de566"
version = "2.8.1+1"

[[deps.IniFile]]
git-tree-sha1 = "f550e6e32074c939295eb5ea6de31849ac2c9625"
uuid = "83e8ac13-25f8-5344-8a64-a9f2b223428f"
version = "0.5.1"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InverseFunctions]]
deps = ["Test"]
git-tree-sha1 = "336cc738f03e069ef2cac55a104eb823455dca75"
uuid = "3587e190-3f89-42d0-90ee-14403ec27112"
version = "0.1.4"

[[deps.IrrationalConstants]]
git-tree-sha1 = "7fd44fd4ff43fc60815f8e764c0f352b83c49151"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.1.1"

[[deps.IterTools]]
git-tree-sha1 = "fa6287a4469f5e048d763df38279ee729fbd44e5"
uuid = "c8e1da08-722c-5040-9ed9-7db0dc04731e"
version = "1.4.0"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.JpegTurbo_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "b53380851c6e6664204efb2e62cd24fa5c47e4ba"
uuid = "aacddb02-875f-59d6-b918-886e6ef4fbf8"
version = "2.1.2+0"

[[deps.LAME_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "f6250b16881adf048549549fba48b1161acdac8c"
uuid = "c1c5ebd0-6772-5130-a774-d5fcae4a789d"
version = "3.100.1+0"

[[deps.LERC_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "bf36f528eec6634efc60d7ec062008f171071434"
uuid = "88015f11-f218-50d7-93a8-a6af411a945d"
version = "3.0.0+1"

[[deps.LZO_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "e5b909bcf985c5e2605737d2ce278ed791b89be6"
uuid = "dd4b983a-f0e5-5f8d-a1b7-129d4a5fb1ac"
version = "2.10.1+0"

[[deps.LaTeXStrings]]
git-tree-sha1 = "f2355693d6778a178ade15952b7ac47a4ff97996"
uuid = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
version = "1.3.0"

[[deps.Latexify]]
deps = ["Formatting", "InteractiveUtils", "LaTeXStrings", "MacroTools", "Markdown", "Printf", "Requires"]
git-tree-sha1 = "46a39b9c58749eefb5f2dc1178cb8fab5332b1ab"
uuid = "23fbe1c1-3f47-55db-b15f-69d7ec21a316"
version = "0.15.15"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.Libffi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "0b4a5d71f3e5200a7dff793393e09dfc2d874290"
uuid = "e9f186c6-92d2-5b65-8a66-fee21dc1b490"
version = "3.2.2+1"

[[deps.Libgcrypt_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libgpg_error_jll", "Pkg"]
git-tree-sha1 = "64613c82a59c120435c067c2b809fc61cf5166ae"
uuid = "d4300ac3-e22c-5743-9152-c294e39db1e4"
version = "1.8.7+0"

[[deps.Libglvnd_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll", "Xorg_libXext_jll"]
git-tree-sha1 = "7739f837d6447403596a75d19ed01fd08d6f56bf"
uuid = "7e76a0d4-f3c7-5321-8279-8d96eeed0f29"
version = "1.3.0+3"

[[deps.Libgpg_error_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "c333716e46366857753e273ce6a69ee0945a6db9"
uuid = "7add5ba3-2f88-524e-9cd5-f83b8a55f7b8"
version = "1.42.0+0"

[[deps.Libiconv_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "42b62845d70a619f063a7da093d995ec8e15e778"
uuid = "94ce4f54-9a6c-5748-9c1c-f9c7231a4531"
version = "1.16.1+1"

[[deps.Libmount_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "9c30530bf0effd46e15e0fdcf2b8636e78cbbd73"
uuid = "4b2f31a3-9ecc-558c-b454-b3730dcb73e9"
version = "2.35.0+0"

[[deps.Libtiff_jll]]
deps = ["Artifacts", "JLLWrappers", "JpegTurbo_jll", "LERC_jll", "Libdl", "Pkg", "Zlib_jll", "Zstd_jll"]
git-tree-sha1 = "c9551dd26e31ab17b86cbd00c2ede019c08758eb"
uuid = "89763e89-9b03-5906-acba-b20f662cd828"
version = "4.3.0+1"

[[deps.Libuuid_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "7f3efec06033682db852f8b3bc3c1d2b0a0ab066"
uuid = "38a345b3-de98-5d2b-a5d3-14cd9215e700"
version = "2.36.0+0"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["ChainRulesCore", "ChangesOfVariables", "DocStringExtensions", "InverseFunctions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "09e4b894ce6a976c354a69041a04748180d43637"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.15"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS]]
deps = ["Dates", "MbedTLS_jll", "Random", "Sockets"]
git-tree-sha1 = "1c38e51c3d08ef2278062ebceade0e46cefc96fe"
uuid = "739be429-bea8-5141-9913-cc70e7f3736d"
version = "1.0.3"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Measures]]
git-tree-sha1 = "e498ddeee6f9fdb4551ce855a46f54dbd900245f"
uuid = "442fdcdd-2543-5da2-b0f3-8c86c306513e"
version = "0.3.1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NaNMath]]
git-tree-sha1 = "737a5957f387b17e74d4ad2f440eb330b39a62c5"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "1.0.0"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.Ogg_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "887579a3eb005446d514ab7aeac5d1d027658b8f"
uuid = "e7412a2a-1a6e-54c0-be00-318e2571c051"
version = "1.3.5+1"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"

[[deps.OpenSSL_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "ab05aa4cc89736e95915b01e7279e61b1bfe33b8"
uuid = "458c3c95-2e84-50aa-8efc-19380b2a3a95"
version = "1.1.14+0"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.Opus_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "51a08fb14ec28da2ec7a927c4337e4332c2a4720"
uuid = "91d4177d-7536-5919-b921-800302f37372"
version = "1.3.2+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.PCRE_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "b2a7af664e098055a7529ad1a900ded962bca488"
uuid = "2f80f16e-611a-54ab-bc61-aa92de5b98fc"
version = "8.44.0+0"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pixman_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "b4f5d02549a10e20780a24fce72bea96b6329e29"
uuid = "30392449-352a-5448-841d-b1acce4e97dc"
version = "0.40.1+0"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlotThemes]]
deps = ["PlotUtils", "Statistics"]
git-tree-sha1 = "8162b2f8547bc23876edd0c5181b27702ae58dce"
uuid = "ccf2f8ad-2431-5c83-bf29-c5338b663b6a"
version = "3.0.0"

[[deps.PlotUtils]]
deps = ["ColorSchemes", "Colors", "Dates", "Printf", "Random", "Reexport", "Statistics"]
git-tree-sha1 = "bb16469fd5224100e422f0b027d26c5a25de1200"
uuid = "995b91a9-d308-5afd-9ec6-746e21dbc043"
version = "1.2.0"

[[deps.Plots]]
deps = ["Base64", "Contour", "Dates", "Downloads", "FFMPEG", "FixedPointNumbers", "GR", "GeometryBasics", "JSON", "Latexify", "LinearAlgebra", "Measures", "NaNMath", "Pkg", "PlotThemes", "PlotUtils", "Printf", "REPL", "Random", "RecipesBase", "RecipesPipeline", "Reexport", "Requires", "Scratch", "Showoff", "SparseArrays", "Statistics", "StatsBase", "UUIDs", "UnicodeFun", "Unzip"]
git-tree-sha1 = "d457f881ea56bbfa18222642de51e0abf67b9027"
uuid = "91a5bcdd-55d7-5caf-9e0b-520d859cae80"
version = "1.29.0"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "47e5f437cc0e7ef2ce8406ce1e7e24d44915f88d"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.3.0"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Qt5Base_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Fontconfig_jll", "Glib_jll", "JLLWrappers", "Libdl", "Libglvnd_jll", "OpenSSL_jll", "Pkg", "Xorg_libXext_jll", "Xorg_libxcb_jll", "Xorg_xcb_util_image_jll", "Xorg_xcb_util_keysyms_jll", "Xorg_xcb_util_renderutil_jll", "Xorg_xcb_util_wm_jll", "Zlib_jll", "xkbcommon_jll"]
git-tree-sha1 = "c6c0f690d0cc7caddb74cef7aa847b824a16b256"
uuid = "ea2cea3b-5b76-57ae-a6ef-0a8af62496e1"
version = "5.15.3+1"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.RecipesBase]]
git-tree-sha1 = "6bf3f380ff52ce0832ddd3a2a7b9538ed1bcca7d"
uuid = "3cdcf5f2-1ef4-517c-9805-6587b60abb01"
version = "1.2.1"

[[deps.RecipesPipeline]]
deps = ["Dates", "NaNMath", "PlotUtils", "RecipesBase"]
git-tree-sha1 = "dc1e451e15d90347a7decc4221842a022b011714"
uuid = "01d81517-befc-4cb6-b9ec-a95719d0359c"
version = "0.5.2"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.RelocatableFolders]]
deps = ["SHA", "Scratch"]
git-tree-sha1 = "cdbd3b1338c72ce29d9584fdbe9e9b70eeb5adca"
uuid = "05181044-ff0b-4ac5-8273-598c1e38db00"
version = "0.1.3"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Scratch]]
deps = ["Dates"]
git-tree-sha1 = "0b4b7f1393cff97c33891da2a0bf69c6ed241fda"
uuid = "6c6a2e73-6563-6170-7368-637461726353"
version = "1.1.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Showoff]]
deps = ["Dates", "Grisu"]
git-tree-sha1 = "91eddf657aca81df9ae6ceb20b959ae5653ad1de"
uuid = "992d4aef-0814-514b-bc4d-f2e9a6c4116f"
version = "1.0.3"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SpecialFunctions]]
deps = ["ChainRulesCore", "IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "bc40f042cfcc56230f781d92db71f0e21496dffd"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.1.5"

[[deps.StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "cd56bf18ed715e8b09f06ef8c6b781e6cdc49911"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.4.4"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.StatsAPI]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "c82aaa13b44ea00134f8c9c89819477bd3986ecd"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.3.0"

[[deps.StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "LogExpFunctions", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "8977b17906b0a1cc74ab2e3a05faa16cf08a8291"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.33.16"

[[deps.StructArrays]]
deps = ["Adapt", "DataAPI", "StaticArrays", "Tables"]
git-tree-sha1 = "9abba8f8fb8458e9adf07c8a2377a070674a24f1"
uuid = "09ab397b-f2b6-538f-b94a-2f83cf4a842a"
version = "0.6.8"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.TensorCore]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "1feb45f88d133a655e001435632f019a9a1bcdb6"
uuid = "62fd8b95-f654-4bbd-a8a5-9c27f68ccd50"
version = "0.1.1"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.UnicodeFun]]
deps = ["REPL"]
git-tree-sha1 = "53915e50200959667e78a92a418594b428dffddf"
uuid = "1cfade01-22cf-5700-b092-accc4b62d6e1"
version = "0.4.1"

[[deps.Unzip]]
git-tree-sha1 = "34db80951901073501137bdbc3d5a8e7bbd06670"
uuid = "41fe7b60-77ed-43a1-b4f0-825fd5a5650d"
version = "0.1.2"

[[deps.Wayland_jll]]
deps = ["Artifacts", "Expat_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Pkg", "XML2_jll"]
git-tree-sha1 = "3e61f0b86f90dacb0bc0e73a0c5a83f6a8636e23"
uuid = "a2964d1f-97da-50d4-b82a-358c7fce9d89"
version = "1.19.0+0"

[[deps.Wayland_protocols_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "4528479aa01ee1b3b4cd0e6faef0e04cf16466da"
uuid = "2381bf8a-dfd0-557d-9999-79630e7b1b91"
version = "1.25.0+0"

[[deps.XML2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libiconv_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "1acf5bdf07aa0907e0a37d3718bb88d4b687b74a"
uuid = "02c8fc9c-b97f-50b9-bbe4-9be30ff0a78a"
version = "2.9.12+0"

[[deps.XSLT_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libgcrypt_jll", "Libgpg_error_jll", "Libiconv_jll", "Pkg", "XML2_jll", "Zlib_jll"]
git-tree-sha1 = "91844873c4085240b95e795f692c4cec4d805f8a"
uuid = "aed1982a-8fda-507f-9586-7b0439959a61"
version = "1.1.34+0"

[[deps.Xorg_libX11_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libxcb_jll", "Xorg_xtrans_jll"]
git-tree-sha1 = "5be649d550f3f4b95308bf0183b82e2582876527"
uuid = "4f6342f7-b3d2-589e-9d20-edeb45f2b2bc"
version = "1.6.9+4"

[[deps.Xorg_libXau_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "4e490d5c960c314f33885790ed410ff3a94ce67e"
uuid = "0c0b7dd1-d40b-584c-a123-a41640f87eec"
version = "1.0.9+4"

[[deps.Xorg_libXcursor_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXfixes_jll", "Xorg_libXrender_jll"]
git-tree-sha1 = "12e0eb3bc634fa2080c1c37fccf56f7c22989afd"
uuid = "935fb764-8cf2-53bf-bb30-45bb1f8bf724"
version = "1.2.0+4"

[[deps.Xorg_libXdmcp_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "4fe47bd2247248125c428978740e18a681372dd4"
uuid = "a3789734-cfe1-5b06-b2d0-1dd0d9d62d05"
version = "1.1.3+4"

[[deps.Xorg_libXext_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "b7c0aa8c376b31e4852b360222848637f481f8c3"
uuid = "1082639a-0dae-5f34-9b06-72781eeb8cb3"
version = "1.3.4+4"

[[deps.Xorg_libXfixes_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "0e0dc7431e7a0587559f9294aeec269471c991a4"
uuid = "d091e8ba-531a-589c-9de9-94069b037ed8"
version = "5.0.3+4"

[[deps.Xorg_libXi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll", "Xorg_libXfixes_jll"]
git-tree-sha1 = "89b52bc2160aadc84d707093930ef0bffa641246"
uuid = "a51aa0fd-4e3c-5386-b890-e753decda492"
version = "1.7.10+4"

[[deps.Xorg_libXinerama_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll"]
git-tree-sha1 = "26be8b1c342929259317d8b9f7b53bf2bb73b123"
uuid = "d1454406-59df-5ea1-beac-c340f2130bc3"
version = "1.1.4+4"

[[deps.Xorg_libXrandr_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll", "Xorg_libXrender_jll"]
git-tree-sha1 = "34cea83cb726fb58f325887bf0612c6b3fb17631"
uuid = "ec84b674-ba8e-5d96-8ba1-2a689ba10484"
version = "1.5.2+4"

[[deps.Xorg_libXrender_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "19560f30fd49f4d4efbe7002a1037f8c43d43b96"
uuid = "ea2f1a96-1ddc-540d-b46f-429655e07cfa"
version = "0.9.10+4"

[[deps.Xorg_libpthread_stubs_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "6783737e45d3c59a4a4c4091f5f88cdcf0908cbb"
uuid = "14d82f49-176c-5ed1-bb49-ad3f5cbd8c74"
version = "0.1.0+3"

[[deps.Xorg_libxcb_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "XSLT_jll", "Xorg_libXau_jll", "Xorg_libXdmcp_jll", "Xorg_libpthread_stubs_jll"]
git-tree-sha1 = "daf17f441228e7a3833846cd048892861cff16d6"
uuid = "c7cfdc94-dc32-55de-ac96-5a1b8d977c5b"
version = "1.13.0+3"

[[deps.Xorg_libxkbfile_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "926af861744212db0eb001d9e40b5d16292080b2"
uuid = "cc61e674-0454-545c-8b26-ed2c68acab7a"
version = "1.1.0+4"

[[deps.Xorg_xcb_util_image_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "0fab0a40349ba1cba2c1da699243396ff8e94b97"
uuid = "12413925-8142-5f55-bb0e-6d7ca50bb09b"
version = "0.4.0+1"

[[deps.Xorg_xcb_util_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libxcb_jll"]
git-tree-sha1 = "e7fd7b2881fa2eaa72717420894d3938177862d1"
uuid = "2def613f-5ad1-5310-b15b-b15d46f528f5"
version = "0.4.0+1"

[[deps.Xorg_xcb_util_keysyms_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "d1151e2c45a544f32441a567d1690e701ec89b00"
uuid = "975044d2-76e6-5fbe-bf08-97ce7c6574c7"
version = "0.4.0+1"

[[deps.Xorg_xcb_util_renderutil_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "dfd7a8f38d4613b6a575253b3174dd991ca6183e"
uuid = "0d47668e-0667-5a69-a72c-f761630bfb7e"
version = "0.3.9+1"

[[deps.Xorg_xcb_util_wm_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "e78d10aab01a4a154142c5006ed44fd9e8e31b67"
uuid = "c22f9ab0-d5fe-5066-847c-f4bb1cd4e361"
version = "0.4.1+1"

[[deps.Xorg_xkbcomp_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libxkbfile_jll"]
git-tree-sha1 = "4bcbf660f6c2e714f87e960a171b119d06ee163b"
uuid = "35661453-b289-5fab-8a00-3d9160c6a3a4"
version = "1.4.2+4"

[[deps.Xorg_xkeyboard_config_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xkbcomp_jll"]
git-tree-sha1 = "5c8424f8a67c3f2209646d4425f3d415fee5931d"
uuid = "33bec58e-1273-512f-9401-5d533626f822"
version = "2.27.0+4"

[[deps.Xorg_xtrans_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "79c31e7844f6ecf779705fbc12146eb190b7d845"
uuid = "c5fb5394-a638-5e4d-96e5-b29de1b5cf10"
version = "1.4.0+3"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.Zstd_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "e45044cd873ded54b6a5bac0eb5c971392cf1927"
uuid = "3161d3a3-bdf6-5164-811a-617609db77b4"
version = "1.5.2+0"

[[deps.libass_jll]]
deps = ["Artifacts", "Bzip2_jll", "FreeType2_jll", "FriBidi_jll", "HarfBuzz_jll", "JLLWrappers", "Libdl", "Pkg", "Zlib_jll"]
git-tree-sha1 = "5982a94fcba20f02f42ace44b9894ee2b140fe47"
uuid = "0ac62f75-1d6f-5e53-bd7c-93b484bb37c0"
version = "0.15.1+0"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.libfdk_aac_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "daacc84a041563f965be61859a36e17c4e4fcd55"
uuid = "f638f0a6-7fb0-5443-88ba-1cc74229b280"
version = "2.0.2+0"

[[deps.libpng_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Zlib_jll"]
git-tree-sha1 = "94d180a6d2b5e55e447e2d27a29ed04fe79eb30c"
uuid = "b53b4c65-9356-5827-b1ea-8c7a1a84506f"
version = "1.6.38+0"

[[deps.libvorbis_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Ogg_jll", "Pkg"]
git-tree-sha1 = "b910cb81ef3fe6e78bf6acee440bda86fd6ae00c"
uuid = "f27f6e37-5d2b-51aa-960f-b287f2bc3b7a"
version = "1.3.7+1"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"

[[deps.x264_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "4fea590b89e6ec504593146bf8b988b2c00922b2"
uuid = "1270edf5-f2f9-52d2-97e9-ab00b5d0237a"
version = "2021.5.5+0"

[[deps.x265_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "ee567a171cce03570d77ad3a43e90218e38937a9"
uuid = "dfaa095f-4041-5dcd-9319-2fabd8486b76"
version = "3.5.0+0"

[[deps.xkbcommon_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Wayland_jll", "Wayland_protocols_jll", "Xorg_libxcb_jll", "Xorg_xkeyboard_config_jll"]
git-tree-sha1 = "ece2350174195bb31de1a63bea3a41ae1aa593b6"
uuid = "d8fb68d0-12a3-5cfd-a85a-d49703b185fd"
version = "0.9.1+5"
"""

# ╔═╡ Cell order:
# ╠═77aedba3-c2c6-430a-9748-f529afb17604
# ╠═9f452e56-de74-11ec-0ef3-6fc65e92a070
# ╟─5b519f65-6894-40ff-a460-b7f4304e066d
# ╠═182ec828-ba1e-4392-b4b7-9df494cf9641
# ╠═bbb64f48-b403-43ec-bd6e-94c042ccadda
# ╠═3149284c-ba3c-4a21-8870-40b5ad8780b3
# ╟─dec55c6b-336d-4a14-ab24-ca877fb45112
# ╟─b7adcff7-b5bf-437c-a48b-b0bf28b7298a
# ╟─f2a602ec-1179-4d6f-90a1-97174db2b263
# ╠═47aaa4f3-27d4-4b0e-a439-6c2b441cc4dd
# ╟─483f0d3f-74d6-4e06-84e2-9bec1ce4fb1f
# ╠═d298e0cc-98aa-430b-b069-408f784292f7
# ╠═7d5327b0-e339-4c1b-a67a-af647df3ff4d
# ╠═cf1d6abd-4a58-4612-984e-35d9fc05546c
# ╟─8a2c18b7-f7ea-46f2-a089-562443438dc1
# ╠═09a48134-e578-4b0e-8b16-a69fe865020c
# ╠═9af7ca8d-02e6-4978-914c-64890813ee66
# ╟─6ea9c88f-32eb-4a3e-97d3-06587d5825c6
# ╟─581fa9b9-0c99-4c50-aff7-98f6233d196b
# ╟─b5831e4a-ff3c-41b0-89ea-489984234041
# ╠═2d99bd64-bc34-4d67-9b88-a1f46c87a66f
# ╟─8d20ec1c-1238-49f9-9dd1-4f220d475e21
# ╟─e3649590-d547-4e77-879a-477b0599f4f5
# ╠═f2b358f5-e327-4958-b72e-329559c27af0
# ╠═b1a21961-beb2-4b10-afb0-3fd8e2723213
# ╠═8d8f3e49-c9a9-4f61-9954-0d071b78610f
# ╟─e8387545-1bf0-41fc-8ff7-90699dc7b1ae
# ╟─80c3b15c-4cb4-4df9-9efd-10889394e0cc
# ╠═b6071ef1-f54c-495b-852d-7df9d6ecd5ee
# ╠═34a81ebd-e509-41bb-a66a-e3a93c3f9752
# ╠═a1eed0e7-ecb6-48a5-850d-ec683f5157bd
# ╠═a8a89f73-689c-4004-a87e-72cb9d9d3c2b
# ╠═e337fa1a-95d6-44ff-903a-caa9d3c9b7c5
# ╟─91405eab-c2b0-405e-9985-58bcb79037aa
# ╟─0181b2a9-5fd6-4d4a-9a4e-18794f034dc3
# ╠═53b78dab-667a-4a87-8f80-10e114082924
# ╟─4ae709e0-c3eb-4b38-8b14-b6fa7e4b88b9
# ╠═e5e8868f-a09d-454a-9bd0-fc6639e92b52
# ╠═979d9ce5-104f-48c4-8292-c3582e7bb87e
# ╠═45ec92dd-8cab-410a-9af9-314024247cac
# ╠═a344797d-1e1e-42e9-9b75-320c521781d6
# ╠═82b72675-4938-4feb-8965-d76ee22ceaed
# ╠═a59467b2-e98a-4589-be43-667951039c88
# ╟─8b247a46-64f2-402a-9a60-d5fc44ddb7e5
# ╠═e11c1475-616d-46f7-aa18-4e29fe55a959
# ╠═7d028810-d9df-49a0-b29a-08a72e9509b9
# ╟─68124cfa-980b-4831-9e5b-e255330e6a8e
# ╠═77ce0322-71ea-444c-8e48-f1a7b52b02d0
# ╠═12873361-902d-4a6f-91bd-6b55c9ea3418
# ╠═3f772d14-db12-4921-8965-f2042782126e
# ╠═a031c9fe-5aa6-4c8b-bfe4-cdf9429336c7
# ╠═75d6aad1-0d2d-418d-b954-665e450e8abe
# ╠═6a2600a1-8e0a-4a95-a72d-120626308530
# ╠═136d4a2f-66e8-429a-9c12-2f50f7d3ec9a
# ╠═b760ec6e-4be9-4221-8d83-5f66507f8c70
# ╠═672888b3-20b9-4243-b36d-cc32079516cd
# ╠═45dd6386-8480-476f-a176-b80ff20a932b
# ╟─46ba433f-8459-4083-a56a-11d1869470c6
# ╠═5f51a124-e1ea-4762-83c6-d55540e69d9c
# ╠═6cfaf34a-0274-4c1d-af85-3f4c1186e2f4
# ╠═ac56b8b7-19a8-4e92-b04c-e94a8076753a
# ╟─fa19ede0-d5e7-4d7c-b973-1e375705e91d
# ╠═d1685688-bfdf-45ba-b485-30a2f5aaf242
# ╟─4895cf21-31eb-4be9-b5e8-144d782d5dbb
# ╠═f25a144f-bed1-481d-8f4f-26855d0c4433
# ╟─e8a93ec5-f4f5-49c2-ac5b-b0a9e190830a
# ╠═b21cbf1e-fd29-4aa3-ba81-d2fb118baceb
# ╟─56cf05e3-6a68-48be-a5ac-173e76cd3490
# ╠═678817c8-1008-4d5e-b748-35cea0f9b79e
# ╟─c09ae70c-0ce8-4c1b-a022-71ee8b9b9d0e
# ╠═9723bce0-a749-40f9-9083-9246f94cd7cc
# ╠═4592285a-6a27-4a0e-a339-1b7dc32cb8f5
# ╠═238447dd-ca0e-401d-b7ea-cbefd621c81e
# ╠═e3e28d3e-ab95-4a85-b6fc-8f00303c75ca
# ╟─23ba5bba-82a2-41c2-9e74-c369a7e44743
# ╠═5e2655e3-72c3-4759-b445-13976d9c4904
# ╠═9b7d8769-c571-448d-afc0-59ea58258557
# ╠═55ea8a8c-5953-48d7-8d50-592e3e052575
# ╟─7851e90d-5bea-4ce0-85a0-a428e62bd78d
# ╠═80965dae-2a75-41b9-885a-8da9b8e42125
# ╟─bed572d5-8ca3-4bbb-b9d1-d14b7f685edf
# ╠═cb64ca85-4d00-42db-8873-410c3ca5197e
# ╠═a6b81141-8d03-4268-a9ae-5f513a0c8f02
# ╠═9c7a7201-eac0-46e7-b757-f0a981b68214
# ╠═7ac1c525-801c-4b19-9708-7b799a1392fe
# ╠═fb70c442-0b69-4505-b147-c33553edc917
# ╟─8e8cc87a-cff1-4d78-b34e-0cd3b3d72de6
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
