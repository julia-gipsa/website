# Tip&Tricks for julia. Sessions 2 of Julia Group, 28 Juin 2022 - by Marco Congedo

using PosDefManifold, Dates, LinearAlgebra, Test, BenchmarkTools


####################################################################################
####################################################################################
####################################################################################


# Basic System functions:

println(" Your Machine `",gethostname(),"` (",Sys.MACHINE, ")")
println(" runs on kernel ",Sys.KERNEL," with word size ",Sys.WORD_SIZE,".")
println(" CPU  Threads: ", Sys.CPU_THREADS)
println(" Base.Threads: ", "$(Threads.nthreads())")
println(" BLAS Threads: ", "$(Sys.CPU_THREADS)", "\n")

## if Base.Threads is smaller than CPU Threads go to File/Preferences/Settings
## and search for 'julia: num'. Then edit the json file


####################################################################################
####################################################################################
####################################################################################


# Iterators start at 1

const die6 = ("⚀", "⚁", "⚂", "⚃", "⚄", "⚅")

function toss2dice6()

    i, j=rand(1:length(die6)), rand(1:length(die6))
    print("\x1b[33m"*die6[i], "  ", die6[j], " ")
    i, j # short for: return (i, j)
end

toss2dice6()

####################################################################################
####################################################################################
####################################################################################

# Essentials

A=randn(20, 20)
B=randn(20, 20); # suppress REPL printing

## like in C you may end a statement with ;, for example:
a=1; println(a)

a isa Float64 # type check, equivalent to
isa(a, Float64)

typeof(A) # type of object
sizeof(A) # size of object, in bytes

methods(sum) # list all available methods for function `sum`

sum((A, B))
which(sum, (typeof((A, B)), )) # individuate the method used giving some arguments

sum([A, B])
which(sum, (typeof([A, B]), ))

a=2
@show a^2+3 a/2; # print multiple expressions

a=3
ans # print the last computed value (like in Matlab)

####################################################################################
####################################################################################
####################################################################################

# Broadcasting and Type casting

C = randn(3, 4).>0

broadcast(x->x^2+3+2, C)



# Lazy Evaluation (&& = 'and', || = 'or')

s=second(now())
isodd(s) && @info "odd second" s

A=randn(2, 3)
B=randn(3, 2)
size(A) == size(B) || throw(DimensionMismatch("size of A is not equal to size of B"))


####################################################################################
####################################################################################
####################################################################################

# Ternary conditional operator (short for if...else conditional expressions)
## Syntax :[condition] ? [execute if true] : [execute if false]

s=second(now())
isodd(s) ? (@info "odd second" s) : (@info "even second" s)

function dummy()
    s=second(now())
    return isodd(s) ? (@info "odd second" s) : (@info "even second" s)
end    

dummy()

####################################################################################
####################################################################################
####################################################################################

# The `splat` operator ...
## 1) indicate an arbitrary sequence of arguments for functions
## 2) apply a function to a sequence of arguments

𝐏=randP(2, 3)

hcat(𝐏...) # 2)
vcat(𝐏...) # 2)

add(xs...) = reduce(+, xs) # 1)
add(4, 1:3...) # 2)
add(4, 1:3..., [1; 4]...) # 2)

####################################################################################
####################################################################################
####################################################################################

# Repeat...Until loops (example of an iterative algorithm)

💡=1 # 10 # initialization
tolerance, maxIter, iter=1e-2, 10, 0
while true
    iter+=1
    y=💡/2 # here the update rule is applied
    conv=y; # here the convergence is evaluated
    println("iteration=$(iter); convergence=$(conv)")
    (overRun = iter == maxIter) && @error "algorithm reached the max number of iterations before convergence:" iter
    conv <= tolerance || overRun ? break : 💡=y
end


####################################################################################
####################################################################################
####################################################################################

# Label...Goto (like in Basic, Pascal,...)

@label name
## ...
@goto name


####################################################################################
####################################################################################
####################################################################################


# The `where` clause in function declaration

## Example: softmax function: p_i=\\frac{\\textrm{e}^{c_i}}{\\sum_{i=1}^{k}\\textrm{e}^{c_i}}
## wikipedia: https://en.wikipedia.org/wiki/Softmax_function
## Suppose

softmax(χ::Vector{T}) where T<:Real = exp.(χ) ./ sum(exp.(χ))

χ=[1.0, 2.3, 0.4, 5.0]
r=softmax(χ)

χ=[1, 2, 5, 6]
r=softmax(χ)

χ=[1//2, 2//2, 3//2, 4//2]
r=softmax(χ)

sum(r)

χ=[1.0, 2.3, 0.4, 5.0].+im
r=softmax(χ)


####################################################################################
####################################################################################
####################################################################################


# C-style low-level functions (the @inbounds macro)

#=    
    For a real or complex positive definite matrix P, let P=LL' be its
    *Cholesky decomposition*, where L is a lower triangular matrix 
    Return the 2-tuple (L, U) such that P=L*L' and inv(P)=U*U'
=#
function choleskyInv!(P::AbstractMatrix{T}) where T<:Real

    n 	= size(P, 1)
    tol = √eps(Float64)
    L₁ 	= LowerTriangular(Matrix{Float64}(I, n, n))
    U₁⁻¹= UpperTriangular(Matrix{Float64}(I, n, n))

    @inbounds   for j=1:n-1
                    P[j, j]<tol && throw(LinearAlgebra.PosDefException(1))
                    for i=j+1:n
                        θ = P[i, j] / -P[j, j]
                        for k=i:n 
                            P[k, i] += θ * P[k, j] 
                        end # update P and write D
                        L₁[i, j] = -θ
                        for k=1:j-1 
                            U₁⁻¹[k, i] += θ * U₁⁻¹[k, j] 
                        end
                        U₁⁻¹[j, i] = θ
                    end
                end
                D=sqrt.(Diagonal(P))
                return L₁*D, U₁⁻¹*inv(D)
end


n = 10 # 300
P=Matrix(randP(n))
Pinv=inv(P)
L, U=choleskyInv!(copy(P))
@test P    ≈ L*L'   # P=L*L'
@test Pinv ≈ U*U'   # P⁻¹=U*U'


# compute the inverse of the cholesky factor using julia's functions
function juliaCholeskyInv!(P::AbstractMatrix{T}) where T<:Real
    C=cholesky!(P)
    return inv(C.L)
end;

Z=Symmetric(P)
Linv=juliaCholeskyInv!(Symmetric(copy(Z)))
@test Linv'*Linv≈inv(Z)

# BenckmarkTools package
@benchmark(choleskyInv!(copy(P)))
@benchmark(juliaCholeskyInv!(copy(Z)))


# see also:

#   @simd (allow compiler to do loop re-ordering)

#   @polly (allow compiler to apply the polyhedral optimizer)

#   @inline (Give a hint to the compiler that this function is worth inlining)
#   @noinline (this function is not worth inlining)


####################################################################################
####################################################################################
####################################################################################

# Array Generators

# 3-tensor generator
A=[i*2j*4l for i=1:3, j=1:3, l=1:3]

## matrix generators
A=[i*2j for i=1:3, j=1:3]

## vector generators

a=[i for i=1:3]

a=[i*2j for i=1:3 for j=1:3]

## conditional generators

a=[i*2j for i=1:3 for j=1:3 if i*2j≠12]

## Example: generate triu(A ⃝ S), where A is a square matrix, S is filled with 1 on the main diagonal 
## and √2 elsewhere, and triu vectorize the upper triangle of a matrix (by rows)

A=[i*2j for i=1:3, j=1:3]

b=[i==j ? A[i, j] : A[i, j]*√2 for i=1:size(A, 1) for j=i:size(A, 2)]

## the above is equivalent to
b=[(if i==j A[i, j] else A[i, j]*√2 end) for i=1:size(A, 1) for j=i:size(A, 2)]

## suppose we want to vectorize only odd rows instead:
b=[i==j ? A[i, j] : A[i, j]*√2 for i=1:size(A, 1) for j=i:size(A, 2) if isodd(i)]

####################################################################################
####################################################################################
####################################################################################

# Linear Algebra operations

## Matlab-style expressions:
A=randn(3, 3)

A' # transpose (Real) or complex-conjugate transpose (complex)

A[1, :]'*A[:, 1] # inner product of first row and first column (return a scalar)

A[1:1, :]*A[:, 1:1] # inner product of first row and first column (return a 1x1 mmatrix)

A[1:2, :]*A[:, 1:2] # product of first two rows and first two columns 


## Algebraic Expressions

𝐏=randP(3, 10) # generate 10 random SPD matrices of size 3x3

### short algebraic expressions, e.g., mean of the log the matrices in an array

mean([log(P) for P∈𝐏]) # don't need to do this

mean(log(P) for P in 𝐏) # you can use comprehensions instead

mean(log(P) for P∈𝐏) # equivalent expression

mean(log(𝐏[i]) for i∈eachindex(𝐏)) # also equivalent

mean(log(𝐏[i]) for i∈1:length(𝐏)) # also equivalent

mean(log.(𝐏)) # using broadcasting instead

mean(log, 𝐏) # using instead a specific method of function `mean`

which(mean, (typeof(log), typeof(𝐏))) # check which method we have invoked

### long algebraic expressions (e.g., inverse of the mean of log of square root of the matrices)

inv(mean(log(√(P)) for P∈𝐏))

inv(mean(log.(sqrt.(𝐏)))) # using broadcasting

(@. 𝐏 |> sqrt |> log) |> mean |> inv # using piping and broadcasting

mean(@. 𝐏 |> sqrt |> log) |> inv # also this way works

# NB: This would instead compute the inverse of the mean of all elements of the matrices,
# resulting in a 10-vector
@. 𝐏 |> sqrt |> log |> mean |> inv

####################################################################################
####################################################################################
####################################################################################

# Quick plots using UnicodePlots.jl or Plots with backend UnicodePlots

using UnicodePlots

barplot(["Paris", "New York", "Moskau", "Madrid"],
        [2.244, 8.406, 11.92, 3.165],
        title="Population")

boxplot(["one", "two"],
        [[1, 2, 3, 4, 5], [2, 3, 4, 5, 6, 7, 8, 9]],
        title="Grouped Boxplot", xlabel="x")

F=eigen(randP(10)) # eigenvalue-eigenvector decomposition
heatmap(reverse(F.vectors, dims=2), colormap=:inferno)
heatmap(reverse(Matrix(Diagonal(F.values)), dims=2), colormap=:inferno)
        

histogram(randn(1_000_000) .* .1, nbins=100, vertical=true)

